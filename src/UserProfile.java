import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.*;

public class UserProfile {
    private static List<String> keywordList = new ArrayList<>();
    private String urlOfInterest;
    public UserProfile(String urlOfInterest){
        this.urlOfInterest = urlOfInterest;

    }

    public List<String> getKeywordList() {
        return keywordList;
    }

    public void UP(String args) throws IOException {

        List<String> stopWords = Arrays.asList("a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "are", "aren't", "as",
                "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't"
                , "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't",
                "has", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself",
                "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's",
                "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours",
                "ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that",
                "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this",
                "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what",
                "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't",
                "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves");

        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos");
        StanfordCoreNLP nlp = new StanfordCoreNLP(props);



        Document document = Jsoup.connect(urlOfInterest).get();
        String body = document.body().text();
        //System.out.println(body);

        CoreDocument coreDocument = new CoreDocument(body);
        nlp.annotate(coreDocument);

        List<CoreLabel> Tokens = coreDocument.tokens();

        List<CoreSentence> sentence = coreDocument.sentences();
        List splitsentences = new ArrayList();

        //pos
        for (CoreSentence token : sentence) {
            splitsentences.add(token.text().toLowerCase());
        }

        //TF calculation

        List lista = new ArrayList();


        //produce list of tokenized words
        for (CoreLabel a : Tokens) {
            if ((!stopWords.contains(a.word().toLowerCase()) && a.word().toLowerCase().matches("[a-zA-Z]+") && !a.word().toLowerCase().matches("[a-z]"))) {  //if statement that removes unwanted characters from the text and makes sure no words that are from myList declared above go in to the new list(essentially removing stop words)


                lista.add(a.word().toLowerCase()); //adds non stop words to list and makes them lower case
            }


        }
        Set set = new HashSet(lista);


        String kw = "";

        //Calculates tf.idf
        Double maxW = 0.1;
        for (Object s : set) {
            int n3 = 0;
            for (int i = 0; i < splitsentences.size(); i++) {

                if (splitsentences.get(i).toString().contains(s.toString())) {     //checks how many sentences contain any given word used to calculate idf later
                    n3++;

                }
            }

            Double fre = Double.valueOf(Collections.frequency(lista, s)) / Double.valueOf(set.size());
            Double idf = Double.valueOf(Math.log(lista.size() / n3)); //calculates idf

            if (fre * idf > maxW) {
                kw = ("\'" + s + "\'" + " with a weight(TF*IDF) of: " + (fre * idf)); //finds most common word

                maxW = fre * idf;
                keywordList.add((String) s);
            }



        }

        System.out.println("Most common word is"+ kw);

        //System.out.println(keywordList);
    }
}