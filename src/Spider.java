

import java.util.*;

public class Spider {
    private static final int MAX_PAGES_TO_SEARCH = 100;
    private Set<String> pagesVisited = new HashSet<String>();
    private List<String> pagesToVisit = new LinkedList<String>();
    private static List<String> urls_to_be_indexed = new ArrayList<>();


    /**
     * Our main launching point for the Spider's functionality. Internally it creates spider legs
     * that make an HTTP request and parse the response (the web page).
     *
     //* @param searchWord - The word or string that you are searching for
     */
    public void search(String url) {

        while (this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
            String currentUrl;
            SpiderLeg leg = new SpiderLeg();
            if (this.pagesToVisit.isEmpty()) {
                currentUrl = url;
                this.pagesVisited.add(url);
            } else {
                currentUrl = this.nextUrl();
            }
            leg.crawl(currentUrl);

            urls_to_be_indexed.add(currentUrl);
            this.pagesToVisit.addAll(leg.getLinks());
        }
        //System.out.println(urls_to_be_indexed);
        System.out.println("\n Visited " + this.pagesVisited.size() + " web page(s)");
        pagesVisited.clear();  //clears pages visited HashSet to prepare the search for the next url
        pagesToVisit.clear(); //clears pages to visist LinkedList to prepare the search for the next url


    }

    public List<String> getUrlsToBeIndexed()

    { return urls_to_be_indexed; }

    /**
     * Returns the next URL to visit (in the order that they were found). We also do a check to make
     * sure this method doesn't return a URL that has already been visited.
     *
     * @return
     */
    private String nextUrl() {
        String nextUrl;
        do {
            nextUrl = this.pagesToVisit.remove(0);
        } while (this.pagesVisited.contains(nextUrl));
        this.pagesVisited.add(nextUrl);
        return nextUrl;
    }

    }
