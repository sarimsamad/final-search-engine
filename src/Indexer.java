
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.document.Field;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.jsoup.Jsoup;


public class Indexer {
    private IndexWriter writer;
    private String searchWord;
    private List<String> searchKeyword;
    private List<String> url_to_be_indexed;
    private static Boolean c = true;
    private static List<String> url_collection;

    private List<String> urls_hit = new ArrayList<>();
    private List<String> keyword_urls = new ArrayList<>();


    public List<String> setKeywordList(List<String> keywordList) {
        this.keywordList = keywordList;
        return keywordList;
    }

    private List<String> keywordList = new ArrayList<>();

    public Indexer(String searchWord, List<String> searchKeyword) {
        this.searchWord = searchWord;
        this.searchKeyword = searchKeyword;

    }

    public List<String> getUrls_hit() {
        return urls_hit;
    }



    public void SearchTerm(String args) throws IOException, ParseException {

        Spider spider = new Spider();

        String sc = searchWord;

        StandardAnalyzer analyzer = new StandardAnalyzer(); // builds an analyzer with default stop words
        Directory index = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter w = new IndexWriter(index, config);


        url_collection = spider.getUrlsToBeIndexed();
        for (int i = 0; i < url_collection.size(); i++) {
            String a = url_collection.get(i);
            org.jsoup.nodes.Document u = Jsoup.connect(a).get();
            String plaintext = u.body().text();
            addDoc(w, plaintext, a);

            }






        System.out.println("These are the urls(index)" + url_collection);

        System.out.println("Search for an item: ");


        Query q = new QueryParser("title", analyzer).parse(sc);


        w.close();

        int hitsPerPage = 50;
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopDocs docs = searcher.search(q, hitsPerPage);
        ScoreDoc[] hits = docs.scoreDocs;


        System.out.println("Found " + hits.length + " matches.");
        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc;
            org.apache.lucene.document.Document d = searcher.doc(docId);
            //System.out.println((i + 1) + ". " + d.get("id") + "\t" + d.get("title"));
            System.out.println((i + 1) + ". " + d.get("id"));
            urls_hit.add(d.get("id"));
        }

        reader.close();

    }

    public void SearchKeywords(String args) throws IOException, ParseException {

        Spider spider = new Spider();
        UserProfile up = new UserProfile(null);

        List<String> zz = setKeywordList(up.getKeywordList());
        System.out.println("Index class 2" + zz);



        StandardAnalyzer analyzer = new StandardAnalyzer(); // builds an analyzer with default stop words
        Directory index = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter w = new IndexWriter(index, config);



        List<String> url_collection = spider.getUrlsToBeIndexed();
        for (int i = 0; i < url_collection.size(); i++) {
            String a = url_collection.get(i);
            org.jsoup.nodes.Document u = Jsoup.connect(a).get();
            String plaintext = u.body().text();
            addDoc(w, plaintext, a);
        }

        System.out.println("These are the urls(index)");



        System.out.println("Search for an item: ");

        for (int y = 0; y < zz.size(); y++){
            Query q = new QueryParser("title", analyzer).parse(zz.get(y));


            w.close();

            int hitsPerPage = 50;
            IndexReader reader = DirectoryReader.open(index);
            IndexSearcher searcher = new IndexSearcher(reader);
            TopDocs docs = searcher.search(q, hitsPerPage);
            ScoreDoc[] hits = docs.scoreDocs;


            System.out.println("Found " + hits.length + " matches for" + zz.get(y));
            for (int i = 0; i < hits.length; ++i) {
                int docId = hits[i].doc;
                org.apache.lucene.document.Document d = searcher.doc(docId);
                //System.out.println((i + 1) + ". " + d.get("id") + "\t" + d.get("title"));
                System.out.println((i + 1) + ". " + d.get("id"));
                urls_hit.add(d.get("id"));
            }

            reader.close();
        }



    }


    private static void addDoc(IndexWriter w, String title, String id) throws IOException {
        org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
        doc.add(new TextField("title", title, Field.Store.YES));
        doc.add(new StringField("id", id, Field.Store.YES));
        w.addDocument(doc);
    }
}

