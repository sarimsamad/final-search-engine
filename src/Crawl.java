import java.util.ArrayList;
import java.util.List;

public class Crawl {

    public void crawl(){
        Spider spider = new Spider();

        List<String> initial_url = new ArrayList<>();
        initial_url.add("https://www.bbc.co.uk/news");
        initial_url.add("https://www.theguardian.com/uk-news");
        initial_url.add("https://www.mirror.co.uk/news/");
        initial_url.add("https://www.express.co.uk/news");
        for (int i = 0; i < initial_url.size(); i++){
            spider.search(initial_url.get(i));
        }

    }
}
