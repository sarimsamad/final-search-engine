
import org.apache.lucene.queryparser.classic.ParseException;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class App extends JPanel {
    private List<String> url_collection = new ArrayList<>();
    private List<String> keyword_collection = new ArrayList<>();
    private JEditorPane resultEditorPane = new JEditorPane();


    public App() {


        JTextField searchword = new JTextField("Enter search word here");
        JTextField profileBuilder = new JTextField("Enter article of interest here");

        JButton button = new JButton("Search");

        JButton button3 = new JButton("Add to profile");

        JButton button4 = new JButton("Search based on interests");


        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new FlowLayout());
        searchPanel.add(searchword);
        searchPanel.add(profileBuilder);
        searchPanel.add(button);
        searchPanel.add(button3);
        searchPanel.add(button4);


        resultEditorPane.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
        resultEditorPane.setEditable(false);


        resultEditorPane.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if(Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }
            }
        });


        JScrollPane resultsPanel = new JScrollPane(resultEditorPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);



        setLayout(new BorderLayout());
        add(searchPanel, BorderLayout.NORTH);
        add(resultsPanel, BorderLayout.CENTER);

        // Add action listener to button[/B]
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                // Execute when button is pressed


                String getTxt = searchword.getText();

                int messageType = JOptionPane.PLAIN_MESSAGE;

                JOptionPane.showMessageDialog(null, getTxt, "Enter search here", messageType);
                Indexer index = new Indexer(getTxt,null);
                try {
                    index.SearchTerm(getTxt);
                    setUrl_collection(index.getUrls_hit());
                    add_urls_to_pane();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

            }

        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String getText2 = profileBuilder.getText();
                int messageType2 = JOptionPane.PLAIN_MESSAGE;
                JOptionPane.showMessageDialog(null,getText2,"Keyword",messageType2);
                UserProfile up = new UserProfile(getText2);
                try {
                    up.UP(getText2);
                    setKeyword_collection(up.getKeywordList());
                    add_keywords_to_pane();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String getTxt = searchword.getText();
                Indexer index2 = new Indexer(getTxt,null);
                try {
                    index2.SearchKeywords(getTxt);
                   setUrl_collection(index2.getUrls_hit());
                   add_urls_to_pane();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

            }
        });

    }

    public void setUrl_collection(List<String> url_collection) {
        this.url_collection = url_collection;
    }

    public void add_urls_to_pane() {
        String resultsString = "";
        System.out.println(url_collection);
        for(String url : url_collection) {
            resultsString += "<a href=\"" + url + "\">" + url + "</a><br>";
        }
        resultEditorPane.setText(resultsString);

    }
    public void setKeyword_collection(List<String> keyword_collection){
        this.keyword_collection = keyword_collection;

    }
    public void add_keywords_to_pane(){
        String keywordResults = "";
        System.out.println(keyword_collection);
        for(String keyword: keyword_collection){
            keywordResults += keyword+" ";
        }
        resultEditorPane.setText(keywordResults);
    }

    public static void main(String[] args) {

        //use windows theme
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        JFrame frame1 = new JFrame("");
        frame1.setSize(900,700);
        frame1.setLayout(new BorderLayout());

        frame1.add(new App(), BorderLayout.CENTER);


        frame1.setVisible(true);

       frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        Crawl crawler = new Crawl();
        crawler.crawl();



    }
}


